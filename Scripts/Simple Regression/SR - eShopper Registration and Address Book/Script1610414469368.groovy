import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (9999999 - 1111111)) + 1111111)

'Declare strings'
String eShopperEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String eShopperFirstNameEnglish = 'AutoTesting'

String eShopperLastNameEnglish = 'ENGeShopper'

String eShopperMobile = '9' + randomNumber

WebUI.openBrowser('')

'Go to WTCTW UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

//Registration
'click register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

WebUI.enhancedClick(findTestObject('Registration/registration_countryCodeDropdownBtn'))

WebUI.enhancedClick(findTestObject('Registration/registration_countryCode_HK'))

'Enter mobile number'
WebUI.setText(findTestObject('OTP/otp_mobileField'), eShopperMobile)

'Click request OTP button'
WebUI.enhancedClick(findTestObject('OTP/otp_requestOTPBtn'))

'Fill in OTP'
WebUI.setText(findTestObject('OTP/otp_otpField'), GlobalVariable.defaultOTP)

'Submit OTP'
WebUI.enhancedClick(findTestObject('OTP/otp_submitOTPBtn'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_firstName'), eShopperFirstNameEnglish)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_lastName'), eShopperLastNameEnglish)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), eShopperEmail)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'Verify welcome text line 1'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false, FailureHandling.CONTINUE_ON_FAILURE)

'Verify welcome text line 2'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_eShopperText, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10)

KeywordLogger logger = new KeywordLogger()

logger.logInfo(eShopperEmail)

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_accountSummaryLink'))

'click the address book link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_AddressBook'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_countryDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_country_HK'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('AddressBook/addressBook_FirstNameField'), eShopperFirstNameEnglish)

WebUI.setText(findTestObject('AddressBook/addressBook_LastNameField'), eShopperLastNameEnglish)

'Enter TH mobile'
WebUI.setText(findTestObject('AddressBook/addressBook_MobileField'), eShopperMobile)

WebUI.setText(findTestObject('AddressBook/addressBook_addressLine1'), '43120')

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_hkAreaDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_hkArea1'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_hkDistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_hkDistrict1'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_AddAddressBtn'))

'check the name was displayed correctly'
WebUI.verifyTextPresent(eShopperFirstNameEnglish, false)

'check the mobile is displayed correctly'
WebUI.verifyTextPresent(eShopperMobile, false)

'edit address'
WebUI.click(findTestObject('AddressBook/addressBook_EditAddressBtn'))

WebUI.setText(findTestObject('AddressBook/addressBook_addressLine1'), '43121')

'update address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

WebUI.verifyTextPresent('43121', false)

'delete address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DeleteAddressBtn'))

WebUI.verifyTextNotPresent('43121', false)

