import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.codehaus.groovy.runtime.InvokerHelper as InvokerHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger logger = new KeywordLogger()

logger.logInfo('HD + Cathay Bank: ' + GlobalVariable.pt_hd_cathay)

logger.logInfo('HD + Union Pay: ' + GlobalVariable.pt_hd_union)

logger.logInfo('HD + NCCC (Redeem): ' + GlobalVariable.pt_hd_nccc_redeem)

logger.logInfo('HD + NCCC (Instalment): ' + GlobalVariable.pt_hd_nccc_instalment)

logger.logInfo('FD + Cathay Bank: ' + GlobalVariable.pt_fd_cathay)

logger.logInfo('FD + Union Pay: ' + GlobalVariable.pt_fd_union)

logger.logInfo('FD + NCCC (Redeem): ' + GlobalVariable.pt_fd_nccc_redeem)

logger.logInfo('FD + NCCC (Instalment): ' + GlobalVariable.pt_fd_nccc_instalment)

logger.logInfo('CCS + Cathay Bank: ' + GlobalVariable.pt_ccs_cathay)

logger.logInfo('CCS + Union Pay: ' + GlobalVariable.pt_ccs_union)

logger.logInfo('CCS + NCCC (Redeem): ' + GlobalVariable.pt_ccs_nccc_redeem)

logger.logInfo('CCS + NCCC (Instalment): ' + GlobalVariable.pt_ccs_nccc_instalment)

logger.logInfo('CCS + Pick pay: ' + GlobalVariable.pt_ccs_pickpay)

logger.logInfo('CCE + Cathay Bank: ' + GlobalVariable.pt_cce_cathay)

logger.logInfo('CCE + Union Pay: ' + GlobalVariable.pt_cce_union)

logger.logInfo('CCE + NCCC (Redeem): ' + GlobalVariable.pt_cce_nccc_redeem)

logger.logInfo('CCE + NCCC (Instalment): ' + GlobalVariable.pt_cce_nccc_instalment)

logger.logInfo('CCE + Pick pay: ' + GlobalVariable.pt_cce_pickpay)

logger.logInfo('CVS + Cathay Bank: ' + GlobalVariable.pt_cvs_cathay)

logger.logInfo('CVS + Union Pay: ' + GlobalVariable.pt_cvs_union)

logger.logInfo('CVS + NCCC (Redeem): ' + GlobalVariable.pt_cvs_nccc_redeem)

logger.logInfo('CVS + NCCC (Instalment): ' + GlobalVariable.pt_cvs_nccc_instalment)

logger.logInfo('CVS + Pick pay: ' + GlobalVariable.pt_cvs_pickpay)

logger.logInfo('Xborder HD + Cathay Bank: ' + GlobalVariable.pt_xborder_hd_cathay)

logger.logInfo('Xborder SF + Cathay Bank: ' + GlobalVariable.pt_xborder_sf_cathay)

'calculate spent point amount'
if (GlobalVariable.pointRedemptionTest) {
    'wait 1 minute for point changes to catch up'
    WebUI.delay(60)

    WebUI.refresh()

    WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

    GlobalVariable.endingPointAmount = ((WebUI.getText(findTestObject('Home/home_pointAmount')).trim()) as int)

    int pointSpent = GlobalVariable.startingPointAmount - GlobalVariable.endingPointAmount

    logger.logInfo((((((('Point spent = ' + pointSpent) + ' (') + GlobalVariable.currencySymbol) + GlobalVariable.redeemAmount) + 
        ' redeemed per order, which is ') + GlobalVariable.pointsPerRedeemAmount) + ' points per order)')
}

logger.logInfo('Point redemption test = ' + GlobalVariable.pointRedemptionTest)
logger.logInfo('eVouchers applied ('+GlobalVariable.eVoucher+') = ' + GlobalVariable.eVoucherTest)

