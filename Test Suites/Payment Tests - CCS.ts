<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment Tests - CCS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>333fb832-bb23-4cf8-a55e-7170a1741c0e</testSuiteGuid>
   <testCaseLink>
      <guid>30169f17-5b43-4c06-a828-68dd80883edb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Member Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62ffa29-a6b7-4fee-924f-072860b00619</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79cd5624-bce5-4f1c-a970-4507589cf37d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4aa3d710-bd38-463d-9c46-c7c0ca86e8cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a3d9fb1-d118-43b2-9ee9-5d47f73b04c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e607899-e3c1-4d38-b0d4-e8653e1016d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Pick pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4b3d7c9-2687-4ada-b606-53a32b111241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d04c017d-48f5-4ccb-9599-581d86273e12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Display Order Numbers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
