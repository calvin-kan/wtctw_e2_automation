<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment Tests - CVS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>52079be1-2404-4e0d-87fd-6c628cad4ef2</testSuiteGuid>
   <testCaseLink>
      <guid>30169f17-5b43-4c06-a828-68dd80883edb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Member Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62ffa29-a6b7-4fee-924f-072860b00619</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79cd5624-bce5-4f1c-a970-4507589cf37d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>54f588ee-e1d0-4b9e-ae73-96ff4c9d7539</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4779afa0-ebcd-4796-8235-808d774b1f2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>134dc99f-ea5e-4d39-8a47-ef394cc1613d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - Pick Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f5dab3a-b1a8-4dcd-a9a6-77810ab1f5d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d04c017d-48f5-4ccb-9599-581d86273e12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Display Order Numbers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
