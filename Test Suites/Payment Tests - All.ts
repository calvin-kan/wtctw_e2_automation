<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment Tests - All</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3b66c612-b41a-4a36-a32b-f4b705cb10c0</testSuiteGuid>
   <testCaseLink>
      <guid>f9af190d-98ee-457b-89d9-ed2ba82642f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Member Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62ffa29-a6b7-4fee-924f-072860b00619</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79cd5624-bce5-4f1c-a970-4507589cf37d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9e5838df-d9e8-44f4-9dc0-abd7fbbbd120</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acd6503e-ef05-4c69-a59c-55e5aca9b65b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c7d353a-df19-4aab-8eec-182566c00f5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e116d1b-a9ff-47a6-9d63-fe9dcf666d1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10522e9d-c0c4-44aa-9f41-b3d723a7d383</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b866abb-95f5-4d63-b30c-039c44b4651a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22e4704c-4f5c-4e8c-a6e8-21a89043c33f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>828b995c-78cb-442e-8769-4895fb44c203</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e538096-3769-45ed-a4f8-fb595058fc38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - Pick Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca5d277a-307e-48ed-965b-b93fd9fe2dde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Payment Tests/CVS/PT - CVS - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16383d70-249a-4580-99bb-6234d6e797b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2d0de24-3a68-4b36-9d98-526df9888c8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc319991-454b-4888-b54a-1717dcaf240c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Pick pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02060ebd-9be4-4c23-9b79-3b6144154a56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f4bfa35-4a97-4dcd-88a9-23a2c9e54ec5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - Cathay Bank Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2da28cb9-5880-4a5d-a576-63f3ea21da5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - NCCC Instalments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b594045e-79bd-447a-90e0-e2a9a19846dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - Pick pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c982170-d6cb-4111-8571-0d271df8ad87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcfc662f-50c4-4b10-88d0-6605d7369d9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Display Order Numbers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
